package com.hcl.oxbank.Utils;

public class ApplicationConstants {
    public static final String Name = "name";
    public static final String Mobile = "mobile";
    public static final String dob = "dob";
    public static final String Gender = "gender";
    public static final String maritalstatus = "maritalstatus";
    public static final String Expense = "expense";
    public static final String Salary = "salary";
    public static final String creditscore = "creditscore";
    public static final String pattern = "dd/mm/yyyy";
    public static final String namenull = "name cannot be null";
    public static final String jsonfrmt = "dd-mm-yyyy";
    public static final String nameempty = "name cannot be empty";
    public static final String mobilenull = "mobile cannot  be null";
    public static final String mobileempty = "mobile cannot be empty";
    public static final String dobempty = "date cannot be empty";
    public static final String dobnull = "date cannot be null";
    public static final String gendernull = "gender cannot be null";
    public static final String customersave = "/customersave";
    public static final String genderempty = "gender cannot be empty";
    public static final String Maritalnull = "MaritalStatus cannot be null";
    public static final String maritalempty = "MaritalStatus cannot be empty";
    public static final String namepattern = "name should be within limit";
    public static final String mobilepattern = "mobile number should be 10digits";
    public static final String datepattern = "check the date format";
    public static final String Creditpattern = "CreditScore should be within the limit";
    public static final String salarypatter = "salary should be within limit 4-7";
    public static final String expensepatter = "expense should be within range 4to7";
    public static final String res = "you have registered successfully";
    public static final String pareser = "Error: Invalid date format";
    public static final String futures = "Error: Future date not allowed";
}